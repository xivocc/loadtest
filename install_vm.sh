#!/bin/sh

adduser loadtest

apt-get update
apt-get install tmux vim build-essential pkg-config git python python-jinja2 libssl-dev libsctp-dev libncurses5-dev libncurses5-dev libnet1-dev libgsl0-dev
wget https://github.com/SIPp/sipp/archive/3.4.0.tar.gz /

tar -xzf 3.4.0.tar.gz
cd sipp-3.4.0

./configure --with-sctp --with-openssl --with-rtpstream --with-gsl
make install

cd ..
rm -rf 3.4.0.tar
rm -rf sipp-3.4.0

su - loadtest
git clone https://github.com/xivo-pbx/xivo-load-tester.git


