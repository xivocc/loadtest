#/bin/sh

factor=0.8
while : 
do
	a=`date +%H%M`
	printf "$a "
	daterate=`grep $a rates |  awk -F ";" '{print $3}'`
 	if [ -n "$daterate" ]
	then
        	rate=`echo "$daterate * $factor" | bc`
        	cmd="cset rate $rate"
		echo "for $a : $daterate * $factor -> $cmd"
		echo $cmd | nc -u -q1 127.0.0.1 8892
	fi
sleep 10
done
