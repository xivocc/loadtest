# -*- coding: UTF-8 -*-

from __future__ import unicode_literals

## global configuration

sipp_remote_host = '192.168.228.234'
sipp_remote_host_internal = '192.168.228.10'

sipp_local_ip = '192.168.228.239'
sipp_call_rate = 2.0
sipp_rate_period_in_ms = 15000
sipp_max_simult_calls = 500 
#sipp_nb_of_calls_before_exit = 10
#sipp_background = False
#sipp_enable_trace_calldebug = True
#sipp_enable_trace_err = True
#sipp_enable_trace_shortmsg = True
#sipp_enable_trace_stat = True


## global scenarios configuration

calling_line = {
    'username': 'loadtester',
    'password': 'loadtester',
}
calling_line_internal = {
    'username': 'internaltests',
    'password': 'internaltests',
}
meanpause =  {
        'distribution': 'fixed',
        'value': '210000',
}
internal_pause =  {
	'distribution': 'fixed',
	'value': '50000',
}
external_pause =  {
        'distribution': 'fixed',
        'value': '60000',
}
pause = {
    'distribution': 'fixed',
    'value': 50000,
}
answertalkpause = { 
    'distribution': 'fixed',
    'value' : 240000,
}
ringpause = {
    'distribution': 'fixed',
    'value': 20000,
}
answerimmediate = {
   'distribution' : 'fixed',
   'value' : 10,
}
ath_ringpause = {
    'distribution': 'fixed',
    'value': 4000,
}
ath_talktime = {
    'distribution': 'fixed',
    'value': 5000,
}
codec = {
        'name': 'ulaw',
        'pt': 0,
        'rtpmap': '0 PCMU/8000',
}

rtp = 'test3s'


## scenarios configuration

scenarios.call_then_hangup_gateway = dict(
    bind_port = 5073,
    calling_line = calling_line,
#    called_extens = ['73551','73552','73553','73554','73555','73556','73557','73558'],
#    called_extens = ['73100','73300','73400','73401','73403','73500','73551','73552','73553','73554','73555','73556','73557','73558','73666','84551','84552','84553','84554','84555','84556','84557','84558'],
    called_extens = ['73100','73300','73400','73401','73403','73500','73551','73552','73553','73554','73555','73556','73557','73558','73666'],
#    called_extens = ['84551','84552','84553','84554','84555','84556','84557','84558'],
    talk_time = meanpause,
    rtp = rtp,
)
scenarios.answer_then_hangup = dict(
    bind_port = 5060,
    ring_time = answerimmediate,
    talk_time = answertalkpause,
    rtp = rtp,
)
scenarios.answer_then_hold = dict(
    bind_port = 5081,
    ring_time = answerimmediate,
    rtp = rtp,
    talk_time = ath_talktime
)
scenarios.internal_call = dict(
    bind_port = 5080,
    calling_line = calling_line_internal,
    called_extens = range(2770, 2799),
    talk_time = internal_pause,
    rtp = rtp,
)
scenarios.external_call = dict(
    bind_port = 5082,
    calling_line = calling_line_internal,
    called_extens = ['73100', '73101', '73102', '73103'],
    talk_time = external_pause,
    rtp = rtp,
)

