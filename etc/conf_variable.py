# -*- coding: UTF-8 -*-

from __future__ import unicode_literals

## global configuration

sipp_remote_host = '192.168.228.234'
sipp_remote_host_internal = '192.168.228.10'

sipp_local_ip = '192.168.228.239'
sipp_call_rate = 2.0
sipp_rate_period_in_ms = 60000
sipp_max_simult_calls = 500
#sipp_nb_of_calls_before_exit = 10
#sipp_background = False
sipp_enable_trace_calldebug = True
sipp_enable_trace_err = True
#sipp_enable_trace_shortmsg = True
#sipp_enable_trace_stat = True


## global scenarios configuration

calling_line = {
    'username': 'loadtester',
    'password': 'loadtester',
}
calling_line_internal = {
    'username': 'internaltests',
    'password': 'internaltests',
}
meanpause =  {
        'distribution': 'normal',
        'mean': 210000,
        'stdev': 90000,
}
internal_pause =  {
	'distribution': 'normal',
	'mean': 50000,
	'stdev': 35000,
}
external_pause =  {
        'distribution': 'normal',
        'mean': 60000,
        'stdev': 3000,
}
transfer_talk =  {
        'distribution': 'normal',
        'mean': 10000,
        'stdev': 3000,
}
pause = {
    'distribution': 'uniform',
    'min': 10000,
    'max': 100000,
}
answertalkpause = { 
    'distribution': 'normal',
    'mean': 240000,
    'stdev': 60000,
}
ringpause = {
    'distribution': 'exponential',
    'mean': 20000,
}
answerimmediate = {
   'distribution' : 'fixed',
   'value' : 10
}
answerinsecond = {
    'distribution': 'uniform',
    'min': 10,
    'max': 1000,
}
ath_ringpause = {
    'distribution': 'exponential',
    'mean': 4000,
}
ath_talktime = {
    'distribution': 'uniform',
    'min': 3000,
    'max': 7000,
}
xfer_caller_talktime = {
    'distribution': 'uniform',
    'min': 36000,
    'max': 46000,
}
xfer_destination_talktime = {
    'distribution': 'uniform',
    'min': 10000,
    'max': 20000,
}
codec = {
        'name': 'ulaw',
        'pt': 0,
        'rtpmap': '0 PCMU/8000',
}
codec_alaw = {
        'name': 'alaw',
        'pt': 8,
        'rtpmap': '8 PCMA/8000',
}

rtp = 'test3s'


## scenarios configuration

scenarios.call_then_hangup_gateway = dict(
    bind_port = 5073,
    calling_line = calling_line,
# 7XXXX are routed to xivo-integration
# 8XXXX are routed to xivo-ast11
#    called_extens = ['73551','73552','73553','73554','73555','73556','73557','73558'],
#    called_extens = ['73100','73300','73400','73401','73403','73500','73551','73552','73553','73554','73555','73556','73557','73558','73666','84551','84552','84553','84554','84555','84556','84557','84558'],
    called_extens = ['73100','73300','73400','73401','73403','73500','73551','73552','73553','73554','73555','73556','73557','73558','73666'],
#    called_extens = ['73100'],
#    called_extens = ['73300','73400','73401','73403','73500','73551','73552','73553','73554','73555','73556','73557','73558','73666'],
#    called_extens = ['84551','84552','84553','84554','84555','84556','84557','84558'],
    talk_time = meanpause,
    rtp = rtp,
)
scenarios.answer_then_hangup = dict(
    bind_port = 5060,
    ring_time = answerimmediate,
    talk_time = answertalkpause,
    rtp = rtp,
)
scenarios.answer_then_hold = dict(
    bind_port = 5081,
    ring_time = answerimmediate,
    rtp = rtp,
    talk_time = ath_talktime
)
scenarios.internal_call = dict(
    bind_port = 5080,
    calling_line = calling_line_internal,
    called_extens = range(2770, 2799),
    talk_time = internal_pause,
    rtp = rtp,
)
scenarios.external_call = dict(
    bind_port = 5082,
    calling_line = calling_line_internal,
    called_extens = ['73100', '73101', '73102', '73103', '73104'],
    talk_time = external_pause,
    rtp = rtp,
)
scenarios.external_call_to_group = dict(
    bind_port = 5085,
    calling_line = calling_line,
    called_extens = ['75000', '75001'],
    # POLARIS called_extens = ['50000', '50001'],
    talk_time = meanpause,
    rtp = rtp,
)
scenarios.xfer_caller = dict(
    bind_port = 5090,
    calling_line = calling_line,
    called_extens = ['73800'],
    talk_time = xfer_caller_talktime,
    codec = codec_alaw,
    rtp = rtp,
    sipp_call_rate = 5.0,
)
scenarios.xfer_agent = dict(
    bind_port = 5091,
    ring_time = answerinsecond,
    rtp = rtp,
)
scenarios.xfer_destination = dict(
    bind_port = 5092,
    ring_time = answerinsecond,
    talk_time = xfer_destination_talktime,
    rtp = rtp,
)
scenarios.answer_then_transfer = dict(
    bind_port = 5083,
    ring_time = answerimmediate,
    talk_time = transfer_talk,
    transfer_exten = 73500,
    xivo_username = 'transfer1',
    xivo_password = '0000',   
    rtp = rtp,
)
scenarios.internal_call_then_send_dtmf = dict(
    bind_port = 5084,
    calling_line = calling_line_internal,
    called_extens = ['2777','2778','2779','2780','2781'],
    talk_time = internal_pause,
    codec = codec,
    rtp = rtp,
)
