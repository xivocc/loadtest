In etc/conf.py, change sipp_local_ip to your IP, then run in tmux/screen :

./load-tester -c ../loadtest/etc/conf.py ../loadtest/scenarios/call-then-hangup-gateway
./load-tester -c ../loadtest/etc/conf.py scenarios/answer-then-hangup
./load-tester -c ../loadtest/etc/conf.py ../loadtest/scenarios/answer-then-hold

cd loadtest/callfreq;./doit.sh

./load-tester -c ../loadtest/etc/conf.py ../loadtest/scenarios/internal-call
./load-tester -c ../loadtest/etc/conf.py ../loadtest/scenarios/external-call
