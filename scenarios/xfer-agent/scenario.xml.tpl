<?xml version="1.0" encoding="ISO-8859-1" ?>
<scenario name="answer gatling">

<recv request="BYE" optional="true" hide="true">
</recv>

<recv request="INVITE">
  <action>
    <ereg regexp=".*" search_in="hdr" header="From:" assign_to="invited_from"/>
    <ereg regexp=".*" search_in="hdr" header="To:" assign_to="invited_to"/>
    <ereg regexp="xivo-autoanswer" search_in="hdr" header="Alert-Info:" assign_to="unhold"/>
  </action>
</recv>

<nop hide="true" test="unhold" next="after_unhold" />

<send>
  <![CDATA[
    SIP/2.0 180 Ringing
    [last_Via:]
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    [last_CSeq:]
    Contact: <sip:[local_ip]:[local_port]>
    Content-Length: 0

  ]]>
</send>

{{ ring_time|sipp_pause }}

<send retrans="500">
  <![CDATA[
    SIP/2.0 200 OK
    [last_Via:]
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    [last_CSeq:]
    Contact: <sip:[local_ip]:[local_port]>
    Content-Type: application/sdp
    Content-Length: [len]

    v=0
    o=user1 53655765 2353687637 IN IP[local_ip_type] [local_ip]
    s=-
    c=IN IP[media_ip_type] [media_ip]
    t=0 0
    m=audio [media_port] RTP/AVP {{ codec['pt'] }}
    a=rtpmap:{{ codec['rtpmap'] }}
    a=ptime:20
    a=sendrecv

  ]]>
</send>

<recv request="ACK">
</recv>


<send retrans="500">
  <![CDATA[
    INVITE sip:[call_number]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    To:[$invited_from]
    From:[$invited_to];tag=[call_number]
    Contact:[$invited_to]
    [last_Call-ID:]
    CSeq: [cseq] INVITE
    Content-Type: application/sdp
    Content-Length: [len]

    v=0
    o=user1 53655766 2353687638 IN IP[local_ip_type] [local_ip]
    s=-
    c=IN IP[media_ip_type] [media_ip]
    t=0 0
    m=audio [media_port] RTP/AVP {{ codec['pt'] }}
    a=rtpmap:{{ codec['rtpmap'] }}
    a=ptime:20
    a=sendonly

  ]]>
</send>

<recv response="100" optional="true">
</recv>

<recv response="200">
</recv>

<send>
  <![CDATA[
    ACK sip:[call_number]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    CSeq: [cseq] ACK
    Content-Length: 0

  ]]>
</send>


<nop hide="true" next="end" />


<label id="after_unhold" />

<send>
  <![CDATA[
    SIP/2.0 180 Ringing
    [last_Via:]
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    [last_CSeq:]
    Contact: <sip:[local_ip]:[local_port]>
    Content-Length: 0

  ]]>
</send>

{{ ring_time|sipp_pause }}

<send retrans="500">
  <![CDATA[
    SIP/2.0 200 OK
    [last_Via:]
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    [last_CSeq:]
    Contact: <sip:[local_ip]:[local_port]>
    Content-Type: application/sdp
    Content-Length: [len]

    v=0
    o=user1 53655765 2353687637 IN IP[local_ip_type] [local_ip]
    s=-
    c=IN IP[media_ip_type] [media_ip]
    t=0 0
    m=audio [media_port] RTP/AVP {{ codec['pt'] }}
    a=rtpmap:{{ codec['rtpmap'] }}
    a=ptime:20
    a=sendrecv

  ]]>
</send>

<recv request="ACK">
</recv>

<recv request="BYE">
</recv>

<send>
  <![CDATA[
    SIP/2.0 200 OK
    [last_Via:]
    [last_To:]
    [last_From:]
    [last_Call-ID:]
    [last_CSeq:]
    Contact: <sip:[local_ip]:[local_port]>
    Content-Length: 0

  ]]>
</send>

<label id="end" />

</scenario>
