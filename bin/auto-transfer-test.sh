#!/bin/bash

XIVO_HOST=$1
XIVO_USER=$2
XIVO_PASSWORD=$3
XIVO_TRANSFER_DEST=$4
# attended or blind
XIVO_TRANSFER_TYPE=$5

echo "Starting ${XIVO_TRANSFER_TYPE} transfer to ${XIVO_TRANSFER_DEST}"

XIVO_TOKEN=`curl -k -X POST -H 'Content-Type: application/json' -u "${XIVO_USER}:${XIVO_PASSWORD}" "https://${XIVO_HOST}:9497/0.1/token" -d '{"backend": "xivo_user", "expiration": 600}' | jq -r ".data.token"`
XIVO_CALL_ID=`curl -k -H "X-Auth-Token: $XIVO_TOKEN" "https://${XIVO_HOST}:9500/1.0/users/me/calls" | jq -r ".items[0].call_id"`

XIVO_TRANSFER_ID=`curl -k -H "X-Auth-Token: $XIVO_TOKEN" -XPOST "https://${XIVO_HOST}:9500/1.0/users/me/transfers" -d "{\"exten\": \"${XIVO_TRANSFER_DEST}\", \"initiator_call\": \"${XIVO_CALL_ID}\", \"flow\": \"${XIVO_TRANSFER_TYPE}\"}" | jq -r ".id"`

if [ "${XIVO_TRANSFER_TYPE}" == "attended" ]; then
   sleep 5
   curl -k -H "X-Auth-Token: $XIVO_TOKEN" -XPUT "https://${XIVO_HOST}:9500/1.0/users/me/transfers/${XIVO_TRANSFER_ID}/complete"
fi

curl -k -H "X-Auth-Token: $XIVO_TOKEN" "https://${XIVO_HOST}:9500/1.0/users/me/calls"
curl -k -H "X-Auth-Token: $XIVO_TOKEN" "https://${XIVO_HOST}:9500/1.0/users/me/transfers"
