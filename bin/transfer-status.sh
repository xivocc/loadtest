#!/bin/bash

XIVO_HOST=$1
XIVO_USER=$2
XIVO_PASSWORD=$3

XIVO_TOKEN=`curl -k -X POST -H 'Content-Type: application/json' -u "${XIVO_USER}:${XIVO_PASSWORD}" "https://${XIVO_HOST}:9497/0.1/token" -d '{"backend": "xivo_user", "expiration": 600}' | jq -r ".data.token"`

echo "XIVO_TOKEN=${XIVO_TOKEN}"
curl -k -H "X-Auth-Token: $XIVO_TOKEN" "https://${XIVO_HOST}:9500/1.0/users/me/calls"
curl -k -H "X-Auth-Token: $XIVO_TOKEN" "https://${XIVO_HOST}:9500/1.0/users/me/transfers"
